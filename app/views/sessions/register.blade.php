@extends('layouts.master')
@section('content')
<div class="container">
	<h1>Login</h1>
	<div class="row">
	<div class="col-md-6 col-md-offset-3">
	{{ Form::open(array('route' =>'register'))}}
	
	<div class="form-group">
		{{Form::label('username', 'Username')}}
		{{Form::text('username', NULL, array('class' => 'form-control', 'placeholder' => 'Username'))}}
		@if ($errors->get('username'))
			<span class="help-block alert alert-danger">
				<ul>
					@foreach($errors->get('username') as $error)
					<li>{{ $error}}</li>
					@endforeach
				</ul>
			</span>
		@endif
	</div>

	<div class="form-group">
		{{Form::label('password', 'Password')}}
		{{Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password'))}}
		@if ($errors->get('password'))
			<span class="help-block alert alert-danger">
				<ul>
					@foreach($errors->get('password') as $error)
					<li>{{ $error}}</li>
					@endforeach
				</ul>
			</span>
		@endif
	</div>

	<div class="form-group">
		{{Form::label('password2', 'Repeat Password')}}
		{{Form::password('password2', array('class' => 'form-control', 'placeholder' => 'Repeat Password'))}}
		@if ($errors->get('password2'))
			<span class="help-block alert alert-danger">
				<ul>
					@foreach($errors->get('password2') as $error)
					<li>{{ $error}}</li>
					@endforeach
				</ul>
			</span>
		@endif
	</div>

	<div class="form-group">
		{{ Form::label('email', 'Email')}}
		{{-- Form::email('email', array('class' => 'form-control', 'placeholder' => 'Email')) --}}
		@if ($errors->get('email'))
			<span class="help-block alert alert-danger">
				<ul>
					@foreach($errors->get('email') as $error)
					<li>{{ $error}}</li>
					@endforeach
				</ul>
			</span>
		@endif
	</div>

	<div class="form-group">
		{{Form::submit('Register', array('class' => 'btn btn-primary btn-block'))}}
	</div>

	{{Form::close()}}
	</div>
	</div>
@stop
<pre>
<!-- {{var_dump(Session::all())}} -->
</pre>

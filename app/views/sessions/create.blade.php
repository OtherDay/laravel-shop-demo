@extends('layouts.master')
@section('content')
<div class="container">
	<h1>Login</h1>
	<div class="row">
	<div class="col-md-6 col-md-offset-3">
	{{ Form::open(array('route' =>'authenticate'))}}
	
	<div class="form-group">
		{{Form::label('username', 'Username')}}
		{{Form::text('username', NULL, array('class' => 'form-control', 'placeholder' => 'Username'))}}
		@if ($errors->get('username'))
			<span class="help-block alert alert-danger">
				<ul>
					@foreach($errors->get('username') as $error)
					<li>{{ $error}}</li>
					@endforeach
				</ul>
			</span>
		@endif
	</div>

	<div class="form-group">
		{{Form::label('password', 'Password')}}
		{{Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password'))}}
		@if ($errors->get('password'))
			<span class="help-block alert alert-danger">
				<ul>
					@foreach($errors->get('password') as $error)
					<li>{{ $error}}</li>
					@endforeach
				</ul>
			</span>
		@endif
	</div>

	<div class="checkbox">
		{{ Form::label('remember_me', "keep me logged in for five years") }}
		{{ Form::checkbox('remember_me', true, false) }}		
	</div>

	<div class="form-group">
		{{Form::submit('Log In', array('class' => 'btn btn-primary btn-block'))}}
	</div>

	{{Form::close()}}
	</div>
	</div>
@stop
<pre>
<!-- {{var_dump(Session::all())}} -->
</pre>

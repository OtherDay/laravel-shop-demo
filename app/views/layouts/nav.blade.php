<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">Shop Demo</a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li><a href="/products">Products</a></li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li><a href="/cart">Cart <span class="glyphicon glyphicon-shopping-cart"></span> <span class="badge">{{ $cartCount }}</span></a></li>
      </ul>
      @if(Auth::check())
                <div class="navbar-form navbar-right">
                    {{ link_to_route('logout', 'Logout', NULL, array('class' => 'btn btn-default')) }}
                </div>
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <img class="pull-left" style="height: 38px; margin: .5rem; padding-right: 2em;" src="{{ Gravatar::src(Auth::user()->email )}}">
                        </li>
                    </ul>
      @else      
      <div class="navbar-form navbar-right">
        {{ link_to_route('register', 'Register', NULL, array('class' => 'btn btn-success')) }}
      </div>
      {{ Form::open(
          array(
              'route' => 'authenticate',
              'class' => 'navbar-form navbar-right',
              'role' => 'form'
          )
      ) }}
          <div class="form-group">
              {{ Form::text('username', NULL, array(
                  'class' => 'form-control',
                  'placeholder' => 'username'
              )) }}
          </div>
          <div class="form-group">
              {{ Form::password('password', array(
                  'class' => 'form-control',
                  'placeholder' => 'password'
              )) }}
          </div>
          <div class="form-group">
              {{ Form::submit('Login', array('class' => 'btn btn-success'))}}
          </div>
      {{ Form::close()}}


      @endif
    </div><!--/.navbar-collapse -->
  </div>
</div>
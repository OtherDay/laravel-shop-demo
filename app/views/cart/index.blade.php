@extends('layouts.master')

@section('page-title') @parent
cart
@stop

@section('content')

<div class="container">
	<h2>le carte Monsieur</h2>
	@if (count($products) > 0)
	{{ Form::open(['route' => 'cart.update', 'method' => 'put']) }}
	<table class="table table-striped table-bordered table-hover">
		<tr>
			<th>Product Name</th>
			<th>Unit Price</th>
			<th>Quantity</th>
			<th>Subtotal</th>
		</tr>
		@foreach ($products as $item)
		<tr>
			<td>
				{{ link_to_route('products.show', $item->product->title, $item->product->id) }}
			</td>
			<td>{{ $item->product->price }}</td>
			<td>{{ Form::text('quantity-cart-' . $item->id, $item->quantity,[
				'class' => 'form-control',
				'size' =>'5'
			]) }} </td>
			<td>{{ $item->total }}</td>
		</tr>
		@endforeach
		<tr>
			<td></td>
			<td></td>
			<td>Total</td>
			<td>{{ $total }}</td>
		</tr>
		<tr>
			<td colspan="2"></td>
			<td>{{ Form::submit('Update Cart', ['class' => 'btn btn-default']) }}</td>
			<td></td>
		</tr>
	</table>
	
	{{ Form::close() }}
	@else
		<p>no products found</p>
	@endif
</div> <!-- /container -->

@stop
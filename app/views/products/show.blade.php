@extends('layouts.master')

@section('page-title') @parent
{{ $product->title }}
@stop

@section('content')

<div class="container">
	<h2>{{ $product->title }}</h2>
	<p>{{ $product->description }}</p>
	<p>${{ $product->price }}</p>
	
	{{ Form::open(['route' => 'cart.store', 'class' => 'form-inline']) }}
		<div class="form-group">
			{{ Form::label('quantity', 'Quantity:') }}
			{{ Form::text('quantity', 1, ['size' => '5', 'class' => 'form-control']) }}
		</div>
		{{ Form::hidden('product_id', $product->id) }}
		{{ Form::submit('Add to Cart', ['class' => 'btn btn-default']) }}
		<span class="text-danger">
		@foreach($errors->all() as $error)
		{{ $error }}
		@endforeach
		</span>
	{{ Form::close() }}

</div> <!-- /container -->

@stop
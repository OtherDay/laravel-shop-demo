<?php

class UserProductCartObserver {
	
	public function saving($model)
	{
		if ($model->quantity <= 0) {
			$model->delete();
			return false;
		}
	}

}
<?php

use Illuminate\Database\Eloquent\Collection;


class UserProductCartCollection extends Collection {

	public function cartTotal(){
		$total = 0;
		foreach ($this as $item) {
			$total += $item->product->price * $item->quantity;
		}
		return $total;
	}

	public function cartQuantity(){
		$itemcount = 0;
		foreach($this as $item) {
			$itemcount += $item->quantity;
		}
		return $itemcount;
	}


}
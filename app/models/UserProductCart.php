<?php

class UserProductCart extends Eloquent {

	var $fillable = ['user_id', 'product_id', 'quantity'];
	var $table = "users_products_cart";

	static $rules = [
		'user_id' => 'required',
		'product_id' => ['required', 'exists:products,id'],
		'quantity' => ['required', 'integer', 'min:1'],
	];

	public static function boot() {
		parent::boot();
		self::observe( new UserProductCartObserver );
	}

	public function product(){
		return $this->belongsTo('Product');
	}
	public function newCollection(array $models = array()) {
		return new UserProductCartCollection($models);
	}
	public function getTotalAttribute() {
		return $this->quantity * $this->product->price;
	}
	static public function currentUser() {
		return self::where('user_id', 1)->get();
	}
	public function setQuantityAttribute($value){
		if ($value != floor($value)) {
			throw new InvalidArgumentException('Quantity must be an integer');
		} else {
			$this->attributes['quantity'] = $value;
		}
	}

}
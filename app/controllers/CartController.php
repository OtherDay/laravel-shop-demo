<?php

class CartController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /cart
	 *
	 * @return Response
	 */
	public function index()
	{
		$user_id = 1;
		$products = UserProductCart::with('product')->where('user_id', $user_id)->get();

		$total = $products->cartTotal();


		return View::make('cart.index', compact('products', 'total'));
	}


	/**
	 * Store a newly created resource in storage.
	 * POST /cart
	 *
	 * @return Response
	 */
	public function store()
	{	
		$user_id = 1;
		$product_id = Input::get('product_id');
		$quantity = Input::get('quantity', 1);
		// $user_id = Input::get('user_id', 'bob');

		$data = compact('user_id', 'product_id', 'quantity');

		$validator = Validator::make($data, UserProductCart::$rules);
		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}

		// $product = Product::findOrFail($product_id);

		//find existing matching product in cart
		$cart_entry = UserProductCart::where('user_id', $user_id)->where('product_id', $product_id)->first();

		if ($cart_entry) {
			$cart_entry->quantity += $quantity;
			$cart_entry->save();
		} else {
			$cart_entry = UserProductCart::create(compact('user_id', 'product_id', 'quantity'));
		}

		$cart_entry->save();


		return Redirect::route('cart.index');
	}

	/**
	 * Display the specified resource.
	 * GET /cart/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /cart/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /cart/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{	
		// return Input::all();
		$userCart = UserProductCart::currentUser();
		foreach (Input::all() as $key => $quantity) {
			if (preg_match('/quantity-cart-/', $key)) {
				$cart_id = explode('-', $key)[2];
				$cartitem = $userCart->find($cart_id);
				$cartitem->quantity = floor($quantity);
				$cartitem->save();
			}
		}
		return Redirect::route('cart.index');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /cart/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
<?php

class UserController extends \BaseController {


	public function create()
	{
		return View::make('sessions.register');
	}


	public function login()
	{
		return View::make('sessions.create');
	}

	public function authenticate()
	{
		if(!Auth::attempt(Input::only('username', 'password'), Input::get('remember_me'))){
		return Redirect::route('login')->withErrors(array(
			'username' =>'Username and/or password incorrect.'
		))->withInput();
		}

		return Redirect::intended();
	}

	public function store()
	{
		return Input::all();
	}

	public function logout()
	{
		Auth::logout();
		return Redirect::back();
	}


}

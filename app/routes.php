<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', ['as' => 'home', function()
{
	$cart = new UserProductCart;
	
	return View::make('home');
}]);


Route::get('cart', ['as' => 'cart.index', 'uses' => 'CartController@index']);
Route::post('cart', ['as' => 'cart.store', 'uses' => 'CartController@store']);
Route::match(['put', 'patch'], 'cart', ['as' => 'cart.update', 'uses' => 'CartController@update']);

Route::resource('products', 'ProductsController');

Route::resource('users', 'UserController');
Route::get('register', ['as' => 'register', 'uses' => 'UserController@create']);
Route::post('register', ['as' => 'register', 'uses' => 'UserController@store']);
Route::get('login', ['as' => 'login', 'uses' => 'UserController@login']);
Route::post('login', ['as' => 'authenticate', 'uses' => 'UserController@authenticate']);
Route::get('logout', ['as' => 'logout', 'uses' => 'UserController@logout']);

View::composer('layouts.master', function($view) {
	$view->with('cartCount', UserProductCart::currentUser()->cartQuantity());
});

App::missing(function($exception)
{
    return Response::view('errors.missing', array(), 404);
});
<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ProductsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		DB::table('products')->truncate();

		foreach(range(1, 50) as $index)
		{
			Product::create([
				'title' => $faker->colorName . ' ' . $faker->lastName,
				'description' => $faker->text,
				'price' => $faker->randomFloat(2, 5, 200),
			]);
		}
	}

}
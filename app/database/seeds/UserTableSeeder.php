<?php

class UserTableSeeder extends Seeder {

	public function run()
	{
		DB::table('users')->truncate();

		User::create(array(
			'username'	=>	'alice',
			'password'	=>	Hash::make('alice'),
			'email'	=>	'alice@example.com'
		));
		User::create(array(
			'username'	=>	'bob',
			'password'	=>	Hash::make('bob'),
			'email'	=>	'bob@example.com'
		));
		User::create(array(
			'username'	=>	'Jonas',
			'password'	=>	Hash::make('123123'),
			'email'	=>	'jonas.ermen@gmail.com'
		));
	}
}
